<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>„Helló, Stroustrup!”</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
      
<section>
<title>EPAM: Java GC</title>
<para>Mutasd be nagy vonalakban hogyan működik Java-ban a GC (Garbage Collector). Lehetséges az 
<literal>OutOfMemoryError</literal> kezelése, ha igen milyen esetekben?</para>
<para>Megoldás forrása: <link xlink:href="https://medium.com/@hasithalgamge/seven-types-of-java-garbage-collectors-6297a1418e82">https://medium.com/@hasithalgamge/seven-types-of-java-garbage-collectors-6297a1418e82</link>, <link xlink:href="https://stackoverflow.com/questions/2679330/catching-java-lang-outofmemoryerror">https://stackoverflow.com/questions/2679330/catching-java-lang-outofmemoryerror</link>, <link xlink:href="https://www.baeldung.com/jvm-garbage-collectors">https://www.baeldung.com/jvm-garbage-collectors</link>.</para>
<para>Java programnyelvben a memóriafoglalás és felszabadítás sokkal egyszerűbben történik, mint az eddig használt nyelvekben, mivel a segítségünkre van a Garbage Collector, amely automatikusan végrehajtja helyettünk ezt a feladatot.
    Mindenképpen előnye a Garbage Collector-nak, hogy a memóriát helyettünk foglalja le és szabadítja fel, ezzel sem kell foglalkoznunk, és a memóriaszivárgás problémája is megoldódik. Sajnos nem csak előnye, hanem hátránya is lehet a Garbage Collectornak, mégpedig az, hogy használja a processzorunkat a folyamat során, így lassítva a programunkat egy jól megírt manuális allokálás, deallokálás. Alapvetően 4 eltérő Garbage Collector-t tudunk megkülönböztetni, most ezekről fogunk beszélni pár szót.
    </para>
<para>Az első, talán legegyszerűbb Garbage Collector, nem más lesz, mint a Serial Garbage Collector. Ez egy főleg egy darab thread-et használó programok számára kialakitott collector, amely a futása alatt, leállítja az összes programban lévő thread-et, ezért nem ajánlott a használata olyan programok esetében, amelyek több thread-et használnak. A használatához tartozó parancs:
<programlisting language="java"><![CDATA[
java -XX:+UseSerialGC -jar Application.java
]]></programlisting>
A következő Collector, a Parallel Garbage Collector, amely az előzővel ellentétben már több thread-et fog felhasználni. Egyezés lesz, viszont az előzővel, hogy a GC futása alatt, itt is szüneteltetésre kerülenk a többi thread-ek. Hogyha ezt a GC-t használjuk, több beállításra is lehetőségünk lesz, mint a maximum GC thread-ek, maximum szüneteltetési idő vagy a heap mérete. Használata:
<programlisting language="java"><![CDATA[
java -XX:+UseParallelGC -jar Application.java
]]></programlisting>
Maximum GC thread-ek beállítása:
<programlisting language="java"><![CDATA[
-XX:ParallelGCThreads=<N>
]]></programlisting>
Maximum szüneteltetési idő(ms) beállítása:
<programlisting language="java"><![CDATA[
-XX:MaxGCPauseMillis=<N>
]]></programlisting>
Heap méretének állítása:
<programlisting language="java"><![CDATA[
-Xmx<N>
]]></programlisting>
Most nézzük meg a CMS Garbage Collector-t, fontos, hogy ez a Collector már a java 15-nem nem érhető el.
Ebben az esetben is több GC thread lesz, viszont itt már kisebb szünetekről tudunk majd beszélni. Ez a GC két esetben fogja megkasztani a programunk működését: a szkennelés közben mikor megjelöli a már nem használt objektumokat, illetve akkor, amikor a GC folyamat mellett párhuzamosan valamilyen változás áll be a heap-ben. Olyan esetekben használt, ahol fontos a teljesítmény és megtudunk spórolni egy kis CPU erőforrást, hisz például jobban használja a CPU-t, mint a Parallel GC, de jobb készenlétet biztosít. Az alábbi argumentum segítségével használhatjuk:
<programlisting language="java"><![CDATA[
java -XX:+USeParNewGC -jar Application.java
]]></programlisting>
Az utolsó alapvető Garbage Collector-unk a G1 nevezetű lesz, amely a Java 7-es verziója óta elérhető és manapság már tekinthető a CMS leváltójának. Ez a GC a heap-et különböző régiókra/particiókra osztja fel és párhuzamosan végez rajtuk Garbage Collection-t. Első fázisban ez a GC is megjelöli a már nem hivatkozott objektumokat, majd pedig a második fázisban kitörli azokat, azzal a particióval kezdve ahol a legtöbb szabad hely található. Használata:
<programlisting language="java"><![CDATA[
java -XX:+UseG1GC -jar Application.java
]]></programlisting>
Az <literal>OutOfMemory</literal> error-t nagyon nehéz elkapni, a legnagyobb esélyünk talán akkor lehet rá, hogy maga az error egy a try blokkon belüli deklarálásnál következik be. 
</para>
</section>


<section>
<title>EPAM: Kind of equal</title>
<para>Adott az alábbi kódrészlet.
<programlisting language="java"><![CDATA[
// Given
String first = "...";
String second = "...";
String third = "...";
// When
var firstMatchesSecondWithEquals = first.equals(second);
var firstMatchesSecondWithEqualToOperator = first == second;
var firstMatchesThirdWithEquals = first.equals(third);
var firstMatchesThirdWithEqualToOperator = first == third;
]]></programlisting>
Változtasd meg a <literal>String third = "...";</literal> sort úgy, hogy a
<literal>firstMatchesSecondWithEquals, firstMatchesSecondWithEqualToOperator,
firstMatchesThirdWithEquals</literal> értéke <literal>true</literal>, a
<literal>firstMatchesThirdWithEqualToOperator</literal> értéke pedig <literal>false</literal> legyen. Magyarázd
meg, mi történik a háttérben.
</para>

<para>
Elsőnek is vegyük a feladat szövegében megadott kódcsipetet és illeszük az be egy Junit test-be és értelmezzük magát a kódot.
<programlisting language="java"><![CDATA[
@Test
    public void testKindofEqualExercise_1() {
        // Given
        String first = "...";
        String second = "...";
        String third = "...";
        // When
        var firstMatchesSecondWithEquals = first.equals(second);
        var firstMatchesSecondWithEqualToOperator = first == second;
        var firstMatchesThirdWithEquals = first.equals(third);
        var firstMatchesThirdWithEqualToOperator = first == third;
        // Then
        assertThat(firstMatchesSecondWithEquals, is(true));
        assertThat(firstMatchesSecondWithEqualToOperator, is(true));
        assertThat(firstMatchesThirdWithEquals, is(true));
        assertThat(firstMatchesThirdWithEqualToOperator, is(true));
    }
]]></programlisting>
Egy Test-et fogun látni a megszokott hármas tagolással(given-when-then), amiben a első(given) részében deklerálva, illetve inicializálva van három darab String típusú változó, melyek mind ugyanazt az értéket veszik fel("..."). A második(when) részben különböző "ellenőrzéseket" láthatunk, ahol érdemes megfigyelni, hogy a deklarálásnál, nem egy konkrét típus használunk, hanem a <literal>var</literal> szócskát. A JDK 10-ben bevezetett var, arra fog szolgálni, hogy ne kelljen a változónk típusát explicit módon megjelölni, hanem a compiler megvizsgálja a deklaráció jobb oldalán található értéket és annak a típusát fogja hozzárendelni a változónkhoz is. Mint, ahogy már ebben a könyben is említve volt, az "==" operátor, illetve az <literal>equals</literal> függvény nem ugyanazt a fajta egyenlőséget fogja vizsgálni. Az "==" memóriacím ellenőrzést hajt végre, tehát akkor fog true értéket visszaadni, hogyha a két objektum-nak megegyezik a memóriacíme, ezzel szemben az equals függvény a típusosztályon belül definiált függvény, amely a String esetében a konkrét értékek egyenlőségét fogja vizsgálni. A különböző teszteket elvégezve megnézzük, hogy mindegyik true értékkel tér-e vissza, amire az alábbi eredményt kapjuk:
<figure> 
            <title>testKindofEqualExercise_1 output</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="../img/kindoftest1.png" scale="45" />
                </imageobject>
                <textobject>
                    <phrase>testKindofEqualExercise_1 output</phrase>
                </textobject>
            </mediaobject>
        </figure>
Természetes érhető, hogy arra a tesztekre, ahol a equals függvényt használtuk, miért kapunk true értéket, hisz mind a három String-ünk ugyanazt vette fel értékül. Az érdekes rész a "==" operátornál fog előjönni, hisz ezeknél is true fogunk kapni, ami azt jelenti hogy a három változónknak azonos memóriacímmel kell rendelkeznie. Ez valóban így fog történni és nem más lesz az okozója, mint az úgynevezett String <literal>interning</literal>. A háttérben a String osztály egyfajta poolozást végez(részben hasonlít a Integer wrapper osztályéhoz), ami azt jelenti, hogy egy tömbben folyamatosan tárolja, hogy milyen értékeket adunk egy Stringnek. Ha egy új String típusú változóhoz, egy olyan értéket akarnánk hozzárendelni, amelyet már felvett egy másik String változónk is, akkor ez az <literal>intern</literal> függvény kerül meghívásra. Ez a függvény pedig nem fog új objektumot léttrehozzni, hanem csak visszaadja a tömb elemeként a már tárolt értéket, így pedig a két változónk azonos memóriacímre fognak mutatni.
</para>
<para>
A feladatunk, ugye az volt, hogy valahogyan úgy megváltoztassuk a <literal>String third = "...";</literal> sort, hogy a <literal>firstMatchesThirdWithEqualToOperator</literal> false legyen. Belátható tehát, hogy valamilyen módon az előbb említett interning--et kell megkerülnünk, amit legegyszerűbben a String konstruktorának az explicit módon való meghívással érhetünk el, hisz így biztosan egy új objektumot kapunk vissza, egy újonnan foglalt memóriacímmel. Nézzük meg ezt a forráskódunkban:
<programlisting language="java"><![CDATA[
@Test
    public void testKindofEqualExercise_2() {
        // Given
        String first = "...";
        String second = "...";
        String third = new String("...");

        // When
        var firstMatchesSecondWithEquals = first.equals(second);
        var firstMatchesSecondWithEqualToOperator = first == second;
        var firstMatchesThirdWithEquals = first.equals(third);
        var firstMatchesThirdWithEqualToOperator = first == third;

        // Then
        assertThat(firstMatchesSecondWithEquals, is(true));
        assertThat(firstMatchesSecondWithEqualToOperator, is(true));
        assertThat(firstMatchesThirdWithEquals, is(true));
        assertThat(firstMatchesThirdWithEqualToOperator, is(false));
    }
]]></programlisting>
Láthatjuk, hogy ténylegesen csak a  konstruktor hívást módosítottuk, tehát a <literal>third</literal> változónk deklarálása már a <literal>String third = new String("...");</literal> sorral történik. A tesztre ahol már az ellenőrizzük, hogy a <literal>firstMatchesThirdWithEqualToOperator</literal> értéke false-e, az alábbi eredményt fogjuk kapni:
<figure> 
            <title>testKindofEqualExercise_2 output</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="../img/kindoftest2.png" scale="45" />
                </imageobject>
                <textobject>
                    <phrase>testKindofEqualExercise_2 output</phrase>
                </textobject>
            </mediaobject>
        </figure>

</para>



</section>


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
</chapter> 
